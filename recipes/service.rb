node['gitlab_consul']['services'].each do |service|
  params = {}
  %i[port address tags check checks].each do |param|
    value = service[param.to_s]
    params[param] = value if value
  end

  consul_definition service['name'] do
    type 'service'
    parameters(params)
    notifies :reload, 'consul_service[consul]', :delayed
  end
end
