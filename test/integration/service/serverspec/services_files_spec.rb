require 'serverspec'

# Required by serverspec
set :backend, :exec

describe 'services configuration files' do
  describe file('/etc/consul/conf.d/redis.json') do
    it { should be_file }
    it { should be_owned_by 'consul' }
    it { should be_mode 644 }

    its(:content_as_json) do
      should include('service' => include('name' => 'redis',
                                          'port' => 6379,
                                          'address' => '10.20.30.40',
                                          'tags' => %w(master redis),
                                          'check' => include('http' => 'http://127.0.0.1:6379/check',
                                                             'interval' => '15s')))
    end
  end

  describe file('/etc/consul/conf.d/node_exporter.json') do
    it { should be_file }
    it { should be_owned_by 'consul' }
    it { should be_mode 644 }

    its(:content_as_json) do
      should include('service' => include('name' => 'node_exporter',
                                          'port' => 9100,
                                          'address' => '10.20.30.40',
                                          'tags' => %w(consul_server node_exporter),
                                          'check' => include('http' => 'http://localhost:9100/metrics',
                                                             'interval' => '30s')))
    end
  end
end
